<?php
namespace chillerlan\QRCode;
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <form method="GET">
            @csrf
                <input type="text" name="fio" placeholder="ФИО" required><br>
                <input type="text" name="tel" placeholder="телефон" required><br>
                <input type="email" name="mail" placeholder="почта" required><br>
                <input type="text" name="company" placeholder="компания"><br>
                <input type="text" name="post" placeholder="должность"><br>
                <input type="text" name="benefit" placeholder="чем я буду полезен"><br>
                <input type="text" name="s_network" placeholder="соц. сеть"><br>
                <button type="submit">сгенерировать qr код</button>
            </form>


            <?php
            //var_export($_GET);
            
            $logo = '<img src="images/logo.png" alt="logo"/>';
            
            
            // var_export($data);
            if(isset($_GET['fio'])){
                $data = [
                    "fio" => $_GET['fio'],
                    "tel" => $_GET['tel'],
                    "mail" => $_GET['mail'],
                    "company" => $_GET['company'],
                    "post" => $_GET['post'],
                    "benefit" => $_GET['benefit'],
                    "s_network" => $_GET['s_network'],
                ];
                
                $text = implode(' ',$data);
                
                $qr = '<img src="'.(new QRCode)->render($text).'" alt="QR Code"/>';

                $content = file_get_contents((new QRCode)->render($text));
                file_put_contents('images/qr.png', $content);  
                
                $image1 = imagecreatefrompng('images/qr.png');
                $image2 = imagecreatefrompng('images/logo.png');
                

                $png1 = imagecreatefrompng('images/qr.png');
                imagealphablending($png1, true);
                imagesavealpha($png1, true);
        
               $png2 = imagecreatefrompng('images/logo.png');
                imagealphablending($png2, true);
                imagesavealpha($png2, true);

                
        
                $width1 = imagesx($png1);
                $width2 = imagesx($png2);
                $height1 = imagesy($png1);
                $height2 = imagesy($png2);

                $x = ($width1 - $width2) / 2;
                $y = ($height1 - $height2) / 2;


               // Накладываем
                imagecopy($png1, $png2, $x, $y, 0, 0, $width2, $height2);
        
        // отображаем изображение
                $result = imagepng($png1, 'images/qr-logo.png');
        //И в $result картинку выводим с хедером

            ?>
                <img src="images/qr-logo.png" alt="qr-code">
            <?php
                
                //unlink('images/qr.png');

            }
            ?>
        </div>
    </body>
</html>
